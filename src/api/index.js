import * as http from './utils/http'

export default {
  // 邮箱注册
  email_register (params) {
    return http.post(
      `user_register`, params
    )
  },
  login (params) {
    return http.post(
      `user_auth`, params
    )
  },
  // 忘记密码
  forgotPassword (params) {
    return http.post(
      `user/password/forgot`, params
    )
  },
  // 绑定设备
  userBindDevice (userId, params) {
    return http.post(
      `user/${userId}/register_device`, params
    )
  },

  /**
   * 生成设备二维码
   */
  genQrcode (productId, deviceId, params) {
    return http.post(
      `product/${productId}/device/${deviceId}/qrcode`, params
    )
  },

  // 二维码订阅
  qrcode_subscribe (userId, params) {
    return http.post(
      `user/${userId}/qrcode_subscribe`, params
    )
  },

  // 获取设备列表
  fetchDeviceList (userId) {
    return http.get(
      `user/${userId}/subscribe/devices`
    )
  },

  /**
   * 获取虚拟设备数据
   * @param  {String} productId 产品Id
   * @param  {String} deviceId 虚拟设备 id
   * @return {Promise}
   */
  getVDevice (productId, deviceId, options) {
    return http.get(
      `product/${productId}/v_device/${deviceId}`, options
    )
  },

  getpost () {
    return http.post(
      'http://kq.yinkunworkgo.com/jfinal-share/jssdk/login'
    )
  }
}
/**
 * API统一入口
 *
 * 调用方式：
    import api from '../api'
    api.product.get(productId).then((res) => {
      // 请求状态
      console.log(res.status)
      // 请求头信息
      console.log(res.headers())
      // 赋值
      this.$set('someData', response.data)
    }, (res) => {
      // 失败回调
    })
 */
