export const options = function (token, headers) {
  headers = headers || {
    'Content-Type': 'application/x-www-form-urlencoded'
  }

  headers['Access-Token'] = token

  return {
    headers
  }
}

export default function (appId) {
  if (!appId) return console.error('没有appId')
  let token = JSON.parse(window.localStorage.pluginsToken)[appId].token
  return options(token)
}
