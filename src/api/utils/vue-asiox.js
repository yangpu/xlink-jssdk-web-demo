import axios from 'axios'

const install = function (Vue, options) {
  if (install.installed) return

  Vue.http = axios

  Object.defineProperties(Vue.prototype, {
    $http: {
      get () {
        return axios
      }
    }
  })
}

export default install
