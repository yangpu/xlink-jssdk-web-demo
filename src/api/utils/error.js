export default function handleError (err) {
  handlePlatformError(err)
}

function handlePlatformError (err) {
  let errData = err.response.data
  let errCode = errData.error.code
  switch (errCode) {
    case 4001007:
      showError('密码错误')
      break
    case 4041019:
      showError('企业邮箱不存在')
      break
    case 4001028:
      showError('企业邮箱不存在')
      break
    case 4001102:
      showError('密码必须包含大小写英文和数字')
      break
    case 4031003:
    case 4031021:
    case 4031022:
      showError('Access-Token过期，请重新登录')
      goLogin()
      break
    case 4041003:
      showError('当前账户不存在')
      break
    case 4041011:
      showError('当前账户不存在')
      break
    case 4041010:
      showError('当前账户不存在')
      break
    case 4041020:
      showError('当前app不存在')
      break
    default:
      showError('请求出错')
  }
}

function showError (msg) {
  console.log(msg)
}

function goLogin () {
  let login = window.location.href.split('#')[0] + '#/login'
  window.location.replace(login)
}
