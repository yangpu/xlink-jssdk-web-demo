import Vue from 'vue'
import Router from 'vue-router'
import Register from '@/views/auth/register'
import Login from '@/views/auth/login'
import FetchPassword from '@/views/auth/fetch-password'
import UserBindDevice from '@/views/qrcode_subscribe'
import Xlink from '@/views/xlink'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    // 登录
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    // 注册
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    // 忘记密码
    {
      path: '/fetch-password',
      name: 'FetchPassword',
      component: FetchPassword
    },
    // 用户绑定设备
    {
      path: '/user-bind-device',
      name: 'UserBindDevice',
      component: UserBindDevice
    },

    // sdk
    {
      path: '/xlink',
      name: 'Xlink',
      component: Xlink
    }
  ]
})
